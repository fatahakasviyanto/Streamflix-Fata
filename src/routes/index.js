import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import MovieDetails from "./MovieDetails";
import Movies from "./movies";
import Navbar from "../components/Navbar";

export default function App() {
  return (
    <Router>
      <div>
        <Navbar />

        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
          <Route path="/about">
            <Movies />
          </Route>
          <Route path="/users">
            <Users />
          </Route>
          <Route exact path="/">
            <Movies />
          </Route>
          <Route exact path="/movie/:id" component={MovieDetails} />
        </Switch>
      </div>
    </Router>
  );
}

function Home() {
  return <h2>Home</h2>;
}

function About() {
  return <h2>About</h2>;
}

function Users() {
  return <h2>Users</h2>;
}
