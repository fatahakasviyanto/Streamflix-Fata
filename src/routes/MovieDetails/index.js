import React from "react";
import MovieDescription from "./components/MovieDescription";
import SimilarMovies from "./components/SimilarMovies";

export default function index(props) {
  const params = props.match.params;
  return (
    <div>
      <MovieDescription id={params.id} />
      <SimilarMovies />
    </div>
  );
}
