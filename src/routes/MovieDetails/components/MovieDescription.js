import React, { useEffect, useState } from "react";

export default function MovieDescription(props) {
  const [movie, setMovie] = useState();

  useEffect(() => {
    const api_key = "475d506c06046d091d84c69ad24d6755";
    fetch(`https://api.themoviedb.org/3/movie/${props.id}?api_key=${api_key}`)
      .then((res) => res.json())
      .then(
        (data) => {
          setMovie(data);
          console.log(data);
        },
        (error) => {
          console.log(error);
        }
      );
  }, []);
  return <div>{JSON.stringify(movie)}</div>;
}
