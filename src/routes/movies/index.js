import React from "react";
import MovieList from "./components/MovieList";

export default function index() {
  return (
    <>
      <MovieList />
    </>
  );
}
