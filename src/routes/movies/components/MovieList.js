import React, { useEffect, useState } from "react";
import style from "./MovieList.module.scss";
import { Card, Button, Container, Col, Row } from "react-bootstrap";
import index from "..";
import { Prev } from "react-bootstrap/esm/PageItem";
import { Link } from "react-router-dom";

export default function MovieList() {
  const [movieList, setMovieList] = useState([]);
  const [page, setPage] = useState(1);

  useEffect(() => {
    const api_key = "475d506c06046d091d84c69ad24d6755";
    fetch(
      `https://api.themoviedb.org/3/movie/now_playing?api_key=${api_key}&page=${page}`
    )
      .then((res) => res.json())
      .then(
        (data) => {
          setMovieList(data.results);
          console.log(data.results);
        },
        (error) => {
          console.log(error);
        }
      );
  }, [page]);
  return (
    <>
      <Container>
        <Row>
          {movieList.map(function (val, index) {
            return (
              <div key={index}>
                <Col xs={6} md={3} lg={2}>
                  <Card className={style.card} style={{ width: "18rem" }}>
                    <Card.Img
                      variant="top"
                      src={
                        "https://www.themoviedb.org/t/p/w440_and_h660_face" +
                        val.poster_path
                      }
                    />
                    <Card.Body>
                      <Card.Title>{val.title}</Card.Title>
                      <Card.Text>{val.overview}</Card.Text>
                      <Link to={"/movie/" + val.id}>
                        <Button variant="primary">About</Button>
                      </Link>
                    </Card.Body>
                  </Card>
                </Col>
              </div>
            );
          })}
        </Row>
        <button
          onClick={() => {
            setPage((prev) => {
              if (prev > 1) {
                return prev - 1;
              } else {
                return prev;
              }
            });
          }}
        >
          Previous
        </button>
        <button
          onClick={() => {
            setPage((prev) => {
              if (prev <= 1000) {
                return prev + 1;
              } else {
                return prev;
              }
            });
          }}
        >
          Next
        </button>
      </Container>

      {page}
    </>
  );
}
